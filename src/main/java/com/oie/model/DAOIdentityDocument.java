package com.oie.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "identitydocument")
public class DAOIdentityDocument {

	@Id
	private String document_number;
	@Column
	private String expiry_date;
	@Column
	private String emission_date;
	@Column
	private String document_type_code;
	public String getDocument_number() {
		return document_number;
	}
	public void setDocument_number(String document_number) {
		this.document_number = document_number;
	}
	public String getExpiry_date() {
		return expiry_date;
	}
	public void setExpiry_date(String expiry_date) {
		this.expiry_date = expiry_date;
	}
	public String getEmission_date() {
		return emission_date;
	}
	public void setEmission_date(String emission_date) {
		this.emission_date = emission_date;
	}
	public String getDocument_type_code() {
		return document_type_code;
	}
	public void setDocument_type_code(String document_type_code) {
		this.document_type_code = document_type_code;
	}

	
	
}