package com.oie.controller;

import java.util.ArrayList;
import java.util.Optional;

import javax.validation.Valid;

import org.hibernate.annotations.common.reflection.java.generics.IdentityTypeEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.oie.dao.DocumentTypeDao;
import com.oie.dao.IdentityDocumentDao;
import com.oie.model.DAODocumentType;
import com.oie.model.DAOIdentityDocument;
import com.oie.response.DocumentType;
import com.oie.response.IdentityDocument;
import com.oie.response.IdentityDocumentBody;
import com.oie.response.IdentityNotFoundException;

import antlr.collections.List;
@CrossOrigin(origins = "http://localhost:8081")
@RestController
public class IdentityDocumentController {

	@Autowired
	private IdentityDocumentDao document;
	@Autowired
	private DocumentTypeDao type;

	@GetMapping({ "/identityDocuments" })
	public Iterable<IdentityDocument> all() {
		ArrayList<IdentityDocument> returnList = new ArrayList<>();
		Iterable<DAOIdentityDocument> lst = document.findAll();
		lst.forEach(i -> {
			returnList.add(setData(i));
		});
		return returnList;
	}

	@GetMapping({ "/identityDocuments/{identityNumber}" })
	public IdentityDocument oneByNumber(@PathVariable String identityNumber) {
		Optional<DAOIdentityDocument> obj = Optional.ofNullable(document.findById(identityNumber).orElseThrow(
				() -> new IdentityNotFoundException("No se encontro el numero de documento " + identityNumber)));
		return setData(obj.get());
	}

	public IdentityDocument setData(DAOIdentityDocument obj) {
		IdentityDocumentBody i = new IdentityDocumentBody();
		i.setNumber(obj.getDocument_number());
		i.setExpiryDate(obj.getExpiry_date());
		i.setEmissionDate(obj.getEmission_date());
		DAODocumentType di = type.findByCode(obj.getDocument_type_code());
		i.setDocumentType(new DocumentType(di.getCode(), di.getName()));
		return new IdentityDocument(i);
	}

	@DeleteMapping({ "/identityDocuments/{identityNumber}" })
	@ResponseStatus(code = HttpStatus.OK, reason = "OK")
	public String deleteByNumber(@PathVariable String identityNumber) {
		Optional<DAOIdentityDocument> obj = Optional.ofNullable(document.findById(identityNumber).orElseThrow(
				() -> new IdentityNotFoundException("No se encontro el numero de documento " + identityNumber)));
		document.delete(obj.get());
		return "numero de identidad elminado";
	}

	@PostMapping({ "/identityDocuments" })
	@ResponseStatus(code = HttpStatus.OK, reason = "OK")
	public String save(@Valid @RequestBody IdentityDocument identidad) {
		DAOIdentityDocument daoIdentity = new DAOIdentityDocument();
		DAODocumentType daoType = new DAODocumentType();
		Optional<DAOIdentityDocument> obj1 = document.findById(identidad.getIdentityDocument().getNumber());
		if (!obj1.isEmpty())
			if (obj1.get() != null) {
				throw new IdentityNotFoundException(
						"Numero de documento " + identidad.getIdentityDocument().getNumber() + " ya existe");
			}
		daoIdentity.setDocument_number(identidad.getIdentityDocument().getNumber());
		daoIdentity.setExpiry_date(identidad.getIdentityDocument().getExpiryDate());
		daoIdentity.setDocument_type_code(identidad.getIdentityDocument().getDocumentType().getCode());
		Optional<DAODocumentType> obj2 = type.findById(identidad.getIdentityDocument().getDocumentType().getCode());
		if (!obj2.isEmpty()) {
			if (obj2.get() == null) {
				// crear el tipo de documento
				try {
					daoType.setCode(identidad.getIdentityDocument().getDocumentType().getCode());
					daoType.setName(identidad.getIdentityDocument().getDocumentType().getName());
					type.save(daoType);
				} catch (Exception e) {
					throw new IdentityNotFoundException(
							"Error al crear el tipo de documento" + e.getCause().getCause().toString());
				}
			}
		} else {
			try {
				daoType.setCode(identidad.getIdentityDocument().getDocumentType().getCode());
				daoType.setName(identidad.getIdentityDocument().getDocumentType().getName());
				type.save(daoType);
			} catch (Exception e) {
				throw new IdentityNotFoundException(
						"Error al crear el tipo de documento" + e.getCause().getCause().toString());
			}
		}
		try {
			document.save(daoIdentity);
		} catch (Exception e) {
			throw new IdentityNotFoundException(e.getCause().getCause().toString());
		}
		Optional<DAOIdentityDocument> obj = Optional.ofNullable(document
				.findById(identidad.getIdentityDocument().getNumber()).orElseThrow(() -> new IdentityNotFoundException(
						"No se pudo crear el numero de identidad " + identidad.getIdentityDocument().getNumber())));
		return "creado con exito";
	}

	@PutMapping({ "/identityDocuments" })
	@ResponseStatus(code = HttpStatus.OK, reason = "OK")
	public String update(@Valid @RequestBody IdentityDocument identidad) {
		DAOIdentityDocument daoIdentity = new DAOIdentityDocument();
		DAODocumentType daoType = new DAODocumentType();
		Optional<DAOIdentityDocument> obj1 = document.findById(identidad.getIdentityDocument().getNumber());
		if (obj1.isEmpty()) {
			throw new IdentityNotFoundException(
					"Numero de documento " + identidad.getIdentityDocument().getNumber() + " no existe");
		} else {
			if (obj1.get() == null) {
				throw new IdentityNotFoundException(
						"Numero de documento " + identidad.getIdentityDocument().getNumber() + " no existe");
			}
		}
		daoIdentity.setDocument_number(identidad.getIdentityDocument().getNumber());
		daoIdentity.setExpiry_date(identidad.getIdentityDocument().getExpiryDate());
		daoIdentity.setDocument_type_code(identidad.getIdentityDocument().getDocumentType().getCode());
		Optional<DAODocumentType> obj2 = type.findById(identidad.getIdentityDocument().getDocumentType().getCode());
		if (!obj2.isEmpty()) {
			if (obj2.get() == null) {
				// crear el tipo de documento
				try {
					daoType.setCode(identidad.getIdentityDocument().getDocumentType().getCode());
					daoType.setName(identidad.getIdentityDocument().getDocumentType().getName());
					type.save(daoType);
				} catch (Exception e) {
					throw new IdentityNotFoundException(
							"Error al crear el tipo de documento" + e.getCause().getCause().toString());
				}
			}
		} else {
			try {
				daoType.setCode(identidad.getIdentityDocument().getDocumentType().getCode());
				daoType.setName(identidad.getIdentityDocument().getDocumentType().getName());
				type.save(daoType);
			} catch (Exception e) {
				throw new IdentityNotFoundException(
						"Error al crear el tipo de documento" + e.getCause().getCause().toString());
			}
		}
		try {
			document.save(daoIdentity);
		} catch (Exception e) {
			throw new IdentityNotFoundException(e.getCause().getCause().toString());
		}
		Optional<DAOIdentityDocument> obj = Optional.ofNullable(document
				.findById(identidad.getIdentityDocument().getNumber()).orElseThrow(() -> new IdentityNotFoundException(
						"No se pudo crear el numero de identidad " + identidad.getIdentityDocument().getNumber())));
		return "actualizado con exito";
	}

}