package com.oie.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.oie.response.IdentityNotFoundException;

@ControllerAdvice
public class IdentityNotFoundAdvice {
	 @ResponseBody
	  @ExceptionHandler(IdentityNotFoundException.class)
	  @ResponseStatus(value = HttpStatus.NOT_FOUND )
	  String employeeNotFoundHandler(IdentityNotFoundException ex) {
	    return ex.getMessage();
	  }
}
