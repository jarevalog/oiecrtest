package com.oie.response;

public class IdentityNotFoundException extends RuntimeException {
	public IdentityNotFoundException(String msg) {
		super(msg);
	}
}
