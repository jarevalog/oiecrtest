package com.oie.response;

public class IdentityDocument {
	IdentityDocumentBody identityDocument;

	public IdentityDocumentBody getIdentityDocument() {
		return identityDocument;
	}

	public void setIdentityDocument(IdentityDocumentBody identityDocument) {
		this.identityDocument = identityDocument;
	}

	public IdentityDocument(IdentityDocumentBody identityDocument) {
		super();
		this.identityDocument = identityDocument;
	}

	public IdentityDocument() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
