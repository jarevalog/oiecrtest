package com.oie.dao;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.oie.model.DAOIdentityDocument;
import com.oie.model.DAOUser;

@Repository
public interface IdentityDocumentDao extends CrudRepository<DAOIdentityDocument, String> {
		//DAOIdentityDocument findByDocument_number(String document_number);	
		}