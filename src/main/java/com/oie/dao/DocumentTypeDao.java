package com.oie.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.oie.model.DAODocumentType;
import com.oie.model.DAOUser;

@Repository
public interface DocumentTypeDao extends CrudRepository<DAODocumentType, String> {
	
	DAODocumentType findByCode(String code);
	
}